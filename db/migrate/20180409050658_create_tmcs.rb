class CreateTmcs < ActiveRecord::Migration[5.1]
  def change
    create_table :tmcs do |t|
      t.string :titulo
      t.string :subtitulo
      t.string :tipo
      t.decimal :valor, precision: 4, scale: 2
      t.datetime :fecha

      t.timestamps
    end
  end
end
