class CreateUfs < ActiveRecord::Migration[5.1]
  def change
    create_table :ufs do |t|
      t.decimal :valor, precision: 7, scale: 2
      t.datetime :fecha

      t.timestamps
    end
  end
end
