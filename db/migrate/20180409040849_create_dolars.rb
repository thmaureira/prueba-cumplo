class CreateDolars < ActiveRecord::Migration[5.1]
  def change
    create_table :dolars do |t|
      t.decimal :valor, precision: 5, scale: 2
      t.datetime :fecha

      t.timestamps
    end
  end
end
