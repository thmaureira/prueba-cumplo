class UfsController < ApplicationController
  before_action :set_uf, only: [:show, :edit, :update, :destroy]

  def leer_sbif
  	require 'net/http'
  	anio = ''
  	if params.has_key?(:anio) 
  		anio = params[:anio].to_i.to_s
  	end
    source = 'https://api.sbif.cl/api-sbifv3/recursos_api/uf/'+anio+'?apikey=16ae5ba44e29644b3b9d9ba60f8ea96b569e7608&formato=json'
    resp = Net::HTTP.get_response(URI.parse(source))
    data = resp.body
    @ufs = ActiveSupport::JSON.decode(data)
    @ufs['UFs'].each do |u|
      valor = u['Valor']
      valor.sub! '.', ''
      valor.sub! ',', '.'
      Uf.where(:valor => BigDecimal.new(valor), :fecha => u['Fecha']).first_or_create
    end
  end
end