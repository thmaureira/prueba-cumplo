class TmcsController < ApplicationController
  before_action :set_tmc, only: [:show, :edit, :update, :destroy]

  def leer_sbif
    require 'net/http'
    anio = ''
    if params.has_key?(:anio) 
      anio = params[:anio].to_i.to_s
    end
    source = 'https://api.sbif.cl/api-sbifv3/recursos_api/tmc/'+anio+'?apikey=16ae5ba44e29644b3b9d9ba60f8ea96b569e7608&formato=json'
    resp = Net::HTTP.get_response(URI.parse(source))
    data = resp.body
    @tmcs = ActiveSupport::JSON.decode(data)
    @tmcs['TMCs'].each do |t|
      valor = t['Valor']
      Tmc.where(:valor => BigDecimal.new(valor), :titulo => t['Titulo'], :subtitulo => t['SubTitulo'], :tipo => t['Tipo'], :fecha => t['Fecha']).first_or_create
    end
  end
end
