class DashboardController < ApplicationController
	def index
		#busqueda por rango de fecha
		if params.has_key?(:rango) 
		  @rango = params[:rango]
	      rango = params[:rango].split(' - ')
	      fecha_inicial = Date.strptime(rango[0], '%m/%d/%Y')
	      fecha_final = Date.strptime(rango[1], '%m/%d/%Y')
	    else
	      fecha_inicial = Date.current.days_ago(6)
	      fecha_final = Date.current
	      @rango = fecha_inicial.strftime("%m/%d/%Y") + ' - ' + fecha_final.strftime("%m/%d/%Y")
	    end

	    #valores dentro del rango
	    @ufs = Uf.where(fecha: (fecha_inicial)..fecha_final).order(fecha: :asc)
	    @dolars = Dolar.where(fecha: (fecha_inicial)..fecha_final).order(fecha: :asc)
	    @tmcs = Tmc.where(fecha: (fecha_inicial)..fecha_final).order(fecha: :asc)
	    if @tmcs.count <= 0
	    	@tmcs = Tmc.where("fecha <= ?",fecha_final).order(fecha: :desc).limit(12)
	    end

	    #agrupacion de tmc por tipo
	    @tmcs_tipo = @tmcs
	    @tmcs_tipo = @tmcs_tipo.group_by{|t| [t.tipo]}
	   
	    #promedios
	    @ufs_avg = Uf.where(fecha: (fecha_inicial)..fecha_final).average(:valor)
	    @dolars_avg = Dolar.where(fecha: (fecha_inicial)..fecha_final).average(:valor)
	    #maximos
	    @ufs_max = Uf.where(fecha: (fecha_inicial)..fecha_final).maximum(:valor)
	    @ufs_date_max = Uf.where(fecha: (fecha_inicial)..fecha_final, valor: @ufs_max).select(:fecha).first
	    @ufs_date_max = @ufs_date_max.fecha
	    @dolars_max = Dolar.where(fecha: (fecha_inicial)..fecha_final).maximum(:valor)
	    @dolars_date_max = Dolar.where(fecha: (fecha_inicial)..fecha_final, valor: @dolars_max).select(:fecha).first
	    @dolars_date_max = @dolars_date_max.fecha
	    #minimos
	    @ufs_min = Uf.where(fecha: (fecha_inicial)..fecha_final).minimum(:valor)
	    @ufs_date_min = Uf.where(fecha: (fecha_inicial)..fecha_final, valor: @ufs_min).select(:fecha).first
	    @ufs_date_min = @ufs_date_min.fecha
	    @dolars_min = Dolar.where(fecha: (fecha_inicial)..fecha_final).minimum(:valor)
	    @dolars_date_min = Dolar.where(fecha: (fecha_inicial)..fecha_final, valor: @dolars_min).select(:fecha).first
	    @dolars_date_min = @dolars_date_min.fecha
	end
end
