class DolarsController < ApplicationController
  before_action :set_dolar, only: [:show, :edit, :update, :destroy]

  def leer_sbif
    require 'net/http'
    anio = ''
    if params.has_key?(:anio) 
      anio = params[:anio].to_i.to_s
    end
    source = 'https://api.sbif.cl/api-sbifv3/recursos_api/dolar/'+anio+'?apikey=16ae5ba44e29644b3b9d9ba60f8ea96b569e7608&formato=json'
    resp = Net::HTTP.get_response(URI.parse(source))
    data = resp.body
    @dolars = ActiveSupport::JSON.decode(data)
    #render plain: ufs.inspect
    @dolars['Dolares'].each do |d|
      valor = d['Valor']
      valor.sub! '.', ''
      valor.sub! ',', '.'
      Dolar.where(:valor => BigDecimal.new(valor), :fecha => d['Fecha']).first_or_create
    end
  end
end