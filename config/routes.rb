Rails.application.routes.draw do
  root 'dashboard#index'
  get 'tmcs/leer_sbif'
  root 'tmcs#leer_sbif'
  resources :tmcs
  get 'dolars/leer_sbif'
  root 'dolars#leer_sbif'
  resources :dolars
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'ufs/leer_sbif'
  root 'ufs#leer_sbif'

  get 'ufs/index'
  resources :ufs
  root 'ufs#index'

end
